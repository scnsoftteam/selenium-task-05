# Selenium training

### Setup

```bash
npm install -g protractor
yarn install
```

```bash
yarn webdriver:update
```

### Run the test

```bash
yarn test --suite <your-test>
```

###### Example

```bash
yarn test --suite test-01
```
