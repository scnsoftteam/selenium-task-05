"use strict";
exports.config = {
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 900000,
    random: false
  },

  capabilities: {
    browserName: 'chrome'
  },

  seleniumAddress: 'http://localhost:4444/wd/hub',

  specs: ['./**/*.spec.ts'],

  suites: {
    "test-01": 'e2e/googleSearch.spec.ts',
    "test-02": 'e2e/googlePreferences.spec.ts'
  },

  getPageTimeout: 600000,
  allScriptsTimeout: 600000,

  beforeLaunch: function() {
    require('ts-node').register({ project: '.' });
  }
};
