import { browser } from 'protractor';

import { GoogleSearchPage } from './page-objects';

import { WaitUtils } from './utils';

// Protractor for Non-Angular application
browser.waitForAngularEnabled(false);

describe('Test 01: Google', () => {
  const google = new GoogleSearchPage();

  beforeAll(async () => {
    await google.get();
  });

  it('should search by `protractor`', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      await google.search('protractor');

      expect(await google.getInputSearch()).toEqual('protractor');
      expect(await google.hasResults()).not.toBe(false);
    } catch (error) {
      fail(error);
    }
  });

  it('should search by `protractor` on Page 3', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      await google.clickPage(3);

      expect(await google.getInputSearch()).toEqual('protractor');
      expect(await google.getCurrentPage()).toEqual('3');
    } catch (error) {
      fail(error);
    }
  });

  it('should search by `jasmine` on Page 2', async () => {
    try {
      // wait until the reCAPTCHA is not present
      await WaitUtils.waitForReCaptchaNotPresent();

      await google.search('jasmine');
      await google.clickPage(2);

      expect(await google.getInputSearch()).toEqual('jasmine');
      expect(await google.getCurrentPage()).toEqual('2');
    } catch (error) {
      fail(error);
    }
  });
});
