/* eslint-disable */
export default function logUriList(results) {
  results.count().then((count) => {
    console.log(`\nFound ${count} results`);
    console.log('---');
  });

  results.each((elm) => {
    elm.$('.r').$('a').getAttribute('href').then((uri) => {
      console.log(uri);
    });
  });
}
