import GooglePreferencesPage from './googlePreferences.page';
import GoogleSearchPage from './googleSearch.page';
import GoogleWebPage from './googleWeb.page';

export {
  GoogleWebPage,
  GoogleSearchPage,
  GooglePreferencesPage,
};
