import { $, browser, By, ElementFinder, WebElement, WebElementPromise } from 'protractor';
import { WaitUtils } from '../utils';

export default class GooglePreferencesPage {
  public buttonSave = $('#form-buttons > .jfk-button-action');
  public langItemLink = $('#langSecLink > a');
  public searchItemLink = $('#srhSecLink > a');
  public langPane = $('#langSec');
  public searchPane = $('#srhSec');
  public langAnchorMore = $('#langanchormore');
  public regionAnchorMore = $('#regionanchormore');

  public async savePreferences() {
    // wait until the "Save" button is ready
    await WaitUtils.waitForReady(this.buttonSave);

    // click "Save" button
    await browser.executeScript(`arguments[0].scrollIntoView();`, this.buttonSave.getWebElement());
    await this.buttonSave.click();

    // wait until the Alert is present
    try {
      await WaitUtils.waitForAlertPresent(1000);
      await browser.switchTo().alert().accept();
    } catch { /* skip */ } finally {
      // wait until the "Preferences" of the page exits
      await WaitUtils.waitForNotURLContains('/preferences');

      // delete all cookies
      await browser.manage().deleteAllCookies();
    }
  }

  public async openLanguages() {
    await WaitUtils.waitForURLContains('/preferences');

    // wait until the "Languages" tab is ready
    await WaitUtils.waitForReady(this.langItemLink);

    // click "Languages"
    await browser.executeScript(`arguments[0].click();`, this.langItemLink.getWebElement());
  }

  public async setLanguage(keyISO: string) {
    await WaitUtils.waitForURLContains('/preferences');

    // wait until the "Languages" pane is visible
    await WaitUtils.waitForVisible(this.langPane);

    // wait until the list radio is visible
    await WaitUtils.waitForVisible(this.langPane.$$('.subsect').first());

    // wait until the "Show more" anchor is ready
    await WaitUtils.waitForReady(this.langAnchorMore);

    // click "Show more" anchor
    await browser.executeScript(`arguments[0].scrollIntoView();`, this.langAnchorMore.getWebElement());
    await this.langAnchorMore.click();

    // wait until the radio button ready
    await WaitUtils.waitForReady($(`[data-value="${keyISO}"]`));

    // click radio button
    await $(`[data-value="${keyISO}"]`).click();
  }

  public async setRegion(keyISO: string) {
    await WaitUtils.waitForURLContains('/preferences');

    // wait until the "Search results" pane is visible
    await WaitUtils.waitForVisible(this.searchPane);

    // wait until the list radio is visible
    await WaitUtils.waitForVisible(this.searchPane.$$('.subsect').last());

    // wait until the "Show more" anchor is ready
    await WaitUtils.waitForReady(this.regionAnchorMore);

    // click "Show more" anchor
    await browser.executeScript(`arguments[0].scrollIntoView();`, this.regionAnchorMore.getWebElement());
    await this.regionAnchorMore.click();

    // wait until the radio button ready
    await WaitUtils.waitForReady($(`[data-value="${keyISO}"]`));

    // click radio button
    await $(`[data-value="${keyISO}"]`).click();
  }
}
