const JasmineConsoleReporter = require('jasmine-console-reporter');
const HtmlReporter = require('protractor-beautiful-reporter');

export default class CustomReporter {
  public static setup() {
    jasmine.getEnv().clearReporters();

    jasmine.getEnv().addReporter(new JasmineConsoleReporter({
      colors: true,
      cleanStack: true,
      verbosity: true,
      listStyle: 'flat', // "flat"|"indent"
      activity: false,
    }));

    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'dist/report',
      screenshotsSubfolder: 'images',
      jsonsSubfolder: 'jsons',
      gatherBrowserLogs: false,
      preserveDirectory: true,
    }).getJasmine2Reporter());
  }
}
